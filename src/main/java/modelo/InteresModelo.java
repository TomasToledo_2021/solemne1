package modelo;

public class InteresModelo {

    private String nombreCliente;
    private int edadCliente;
    private double montoInvertir;
    private int añosSolicitud;
    private double calculoInteres;
    private double resultadoFinal;

    public String getNombreCliente() {
        return nombreCliente;
    }

    public int getEdadCliente() {
        return edadCliente;
    }

    public double getMontoInvertir() {
        return montoInvertir;
    }

    public int getAñosSolicitud() {
        return añosSolicitud;
    }

    public double getCalculoInteres() {
        if (añosSolicitud != 0) {
             calculoInteres = (this.montoInvertir * 0.15 * this.añosSolicitud);
        } else {
            calculoInteres = 0;
        }
     
        return calculoInteres;
    }

    public double getResultadoFinal() {
        double valorFinal = (montoInvertir + calculoInteres);
        return valorFinal;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public void setEdadCliente(int edadCliente) {
        this.edadCliente = edadCliente;
    }

    public void setmontoInvertir(double montoInvertir) {
        this.montoInvertir = montoInvertir;
    }

    public void setAñosSolicitud(int añosSolicitud) {
        this.añosSolicitud = añosSolicitud;
    }

    public void setCalculoInteres(double calculoInteres) {
        this.calculoInteres = calculoInteres;
    }

    public void setResultadoFinal(double resultadoFinal) {
        this.resultadoFinal = resultadoFinal;
    }
}
